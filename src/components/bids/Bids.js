import React from "react";
import "./bids.css";
import { AiFillHeart, AiOutlineHeart } from "react-icons/ai";
import bids1 from "../../assets/bid1.jpg";
import bids2 from "../../assets/bid2.jpg";
import bids3 from "../../assets/bid3.jpg";
import bids4 from "../../assets/bid4.jpg";
import bids5 from "../../assets/bid5.jpg";
import bids6 from "../../assets/bid6.jpg";
import bids7 from "../../assets/bid7.jpg";
import bids8 from "../../assets/bid8.jpg";
import { Link } from "react-router-dom";
import { ethers } from "ethers";
import { useEffect, useState } from "react";
import axios from "axios";
import Web3Modal from "web3modal";

// import { marketplaceAddress } from "../config";
import NFTMarketplace from "../../artifacts/contracts/NFTMarketplace.sol/NFTMarketplace.json";
import { marketplaceAddress } from "../../config";
// const marketplaceAddress = "0x56cC10FC78D507AE77cb711A353A4a61058f994C";

const Bids = ({ title }) => {
  const [nfts, setNfts] = useState([]);
  const [loadingState, setLoadingState] = useState("not-loaded");
  useEffect(() => {
    loadNFTs();
  }, []);
  async function loadNFTs() {
    const web3Modal = new Web3Modal({
      network: "rinkeby",
      cacheProvider: true,
    });
    const connection = await web3Modal.connect();
    const provider = new ethers.providers.Web3Provider(connection);
    const signer = provider.getSigner();

    const contract = new ethers.Contract(
      marketplaceAddress,
      NFTMarketplace.abi,
      signer
    );
    const data = await contract.fetchItemsListed();

    const items = await Promise.all(
      data.map(async (i) => {
        const tokenUri = await contract.tokenURI(i.tokenId);
        const meta = await axios.get(tokenUri);
        let price = ethers.utils.formatUnits(i.price.toString(), "ether");
        let item = {
          price,
          tokenId: i.tokenId.toNumber(),
          seller: i.seller,
          owner: i.owner,
          image: meta.data.image,
        };
        return item;
      })
    );
    console.log("NFTs fetched", items);
    setNfts(items);
    setLoadingState("loaded");
  }

  return (
    <div className="bids section__padding">
      <div className="bids-container">
        <div className="bids-container-text">
          <h1>{title}</h1>
        </div>

        <div className="bids-container-card">
          {nfts.map((nft, i) => (
            <div className="card-column">
              <div className="bids-card">
                <div className="bids-card-top">
                  <img src={nft.image} alt="" />
                  <Link to={`/post/123`}>
                    <p className="bids-title">Abstact Smoke Red</p>
                  </Link>
                </div>
                <div className="bids-card-bottom">
                  <p>
                    {nft.price} <span>ETH</span>
                  </p>
                  <p>
                    {" "}
                    <AiFillHeart /> 92
                  </p>
                </div>
              </div>
            </div>
          ))}
          <div className="card-column">
            <div className="bids-card">
              <div className="bids-card-top">
                <img src={bids2} alt="" />
                <Link to={`/post/123`}>
                  <p className="bids-title">Mountain Landscape</p>
                </Link>
              </div>
              <div className="bids-card-bottom">
                <p>
                  0.20 <span>ETH</span>
                </p>
                <p>
                  {" "}
                  <AiFillHeart /> 25
                </p>
              </div>
            </div>
          </div>
          <div className="card-column">
            <div className="bids-card">
              <div className="bids-card-top">
                <img src={bids3} alt="" />
                <Link to={`/post/123`}>
                  <p className="bids-title">Paint Color on Wall</p>
                </Link>
              </div>
              <div className="bids-card-bottom">
                <p>
                  0.55 <span>ETH</span>
                </p>
                <p>
                  {" "}
                  <AiFillHeart /> 55
                </p>
              </div>
            </div>
          </div>
          <div className="card-column">
            <div className="bids-card">
              <div className="bids-card-top">
                <img src={bids4} alt="" />
                <Link to={`/post/123`}>
                  <p className="bids-title">Abstract Patern</p>
                </Link>
              </div>
              <div className="bids-card-bottom">
                <p>
                  0.87 <span>ETH</span>
                </p>
                <p>
                  {" "}
                  <AiFillHeart /> 82
                </p>
              </div>
            </div>
          </div>
          <div className="card-column">
            <div className="bids-card">
              <div className="bids-card-top">
                <img src={bids5} alt="" />
                <Link to={`/post/123`}>
                  <p className="bids-title">White Line Grafiti</p>
                </Link>
              </div>
              <div className="bids-card-bottom">
                <p>
                  0.09 <span>ETH</span>
                </p>
                <p>
                  {" "}
                  <AiFillHeart /> 22
                </p>
              </div>
            </div>
          </div>
          <div className="card-column">
            <div className="bids-card">
              <div className="bids-card-top">
                <img src={bids6} alt="" />
                <Link to={`/post/123`}>
                  <p className="bids-title">Abstract Triangle</p>
                </Link>
              </div>
              <div className="bids-card-bottom">
                <p>
                  0.90 <span>ETH</span>
                </p>
                <p>
                  {" "}
                  <AiFillHeart /> 71
                </p>
              </div>
            </div>
          </div>
          <div className="card-column">
            <div className="bids-card">
              <div className="bids-card-top">
                <img src={bids7} alt="" />
                <Link to={`/post/123`}>
                  <p className="bids-title">Lake Landscape</p>
                </Link>
              </div>
              <div className="bids-card-bottom">
                <p>
                  0.52 <span>ETH</span>
                </p>
                <p>
                  {" "}
                  <AiFillHeart /> 63
                </p>
              </div>
            </div>
          </div>
          <div className="card-column">
            <div className="bids-card">
              <div className="bids-card-top">
                <img src={bids8} alt="" />
                <Link to={`/post/123`}>
                  <p className="bids-title">Blue Red Art</p>
                </Link>
              </div>
              <div className="bids-card-bottom">
                <p>
                  0.85 <span>ETH</span>
                </p>
                <p>
                  {" "}
                  <AiFillHeart /> 66
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="load-more">
        <button>Load More</button>
      </div>
    </div>
  );
};

export default Bids;
