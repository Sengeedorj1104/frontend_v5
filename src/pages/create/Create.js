import "./create.css";
import Image from "../../assets/Image.png";
import { useState } from "react";
import { ethers } from "ethers";
import { create as ipfsHttpClient } from "ipfs-http-client";
import { useRouter } from "next/router";
import Web3Modal from "web3modal";

import { marketplaceAddress } from "../../config";

import NFTMarketplace from "../../artifacts/contracts/NFTMarketplace.sol/NFTMarketplace.json";

const client = ipfsHttpClient("https://ipfs.infura.io:5001/api/v0");

const Create = () => {
  const [fileUrl, setFileUrl] = useState("");
  const [file, setFile] = useState(null);
  const [formData, setFormData] = useState(null);
  const [formInput, updateFormInput] = useState({
    price: "",
    name: "",
    description: "",
    image: "",
  });

  const router = useRouter();

  // async function onChange(e) {
  //   // const file = e.target.files[0];
  //   setFile(e.target.files[0]);
  //   console.log(file);
  //   try {
  //     const added = await client.add(file, {
  //       progress: (prog) => console.log(`received: ${prog}`),
  //     });
  //     console.log("url", fileUrl);
  //     const url = `https://ipfs.infura.io/ipfs/${added.path}`;
  //     setFileUrl(url);
  //   } catch (error) {
  //     console.log("Error uploading file: ", error);
  //   }
  // }
  // async function uploadToIPFS() {
  //   const { name, description, price } = formInput;
  //   if (!name || !description || !price || !fileUrl) return;
  //   /* first, upload to IPFS */
  //   const data = JSON.stringify({
  //     name,
  //     description,
  //     image,
  //   });
  //   try {
  //     const added = await client.add(data);
  //     const url = `https://ipfs.infura.io/ipfs/${added.path}`;
  //     console.log("url", url);
  //     /* after file is uploaded to IPFS, return the URL to use it in the transaction */
  //     return url;
  //   } catch (error) {
  //     console.log("Error uploading file: ", error);
  //   }
  // }

  async function listNFTForSale(e) {
    e.preventDefault();
    console.log("list NFT");
    // const url = await uploadToIPFS();
    const url = fileUrl;
    const web3Modal = new Web3Modal();
    const connection = await web3Modal.connect();
    const provider = new ethers.providers.Web3Provider(connection);
    const signer = provider.getSigner();

    /* next, create the item */
    const price = ethers.utils.parseUnits(formInput.price, "ether");
    // const image = ethers.utils.parseUnits(formInput.image);
    let contract = new ethers.Contract(
      marketplaceAddress,
      NFTMarketplace.abi,
      signer
    );
    let listingPrice = await contract.getListingPrice();
    console.log(listingPrice, url);

    //   {
    //   "name": "Herbie Starbelly",
    //   "description": "Friendly OpenSea Creature that enjoys long swims in the ocean.",
    //   "image": "https://storage.googleapis.com/opensea-prod.appspot.com/creature/50.png",
    //   "attributes": [...]
    // }

    listingPrice = listingPrice.toString();
    let transaction = await contract.createToken(
      formInput.image,
      formInput.description,
      price,
      {
        value: listingPrice,
      }
    );
    await transaction.wait();
    console.log("transaction ", transaction);

    // router.push("/");
  }

  // async function onFileChange(e) {
  //   // Update the state
  //   // this.setState({ selectedFile: e.target.files[0] });
  //   setFile(e.target.files[0]);
  //   console.log("asfsdf", file);
  // }

  return (
    <div className="create section__padding">
      <div className="create-container">
        <h1>Create new Item</h1>
        <p className="upload-file">Upload File</p>
        {/* <div className="upload-img-show">
            <h3>JPG, PNG, GIF, SVG, WEBM, MP3, MP4. Max 100mb.</h3>
            <img src={Image} alt="banner" />
            <p>Drag and Drop File</p>
          </div> */}
        <form className="writeForm" autoComplete="off">
          {/* <div className="formGroup">
            <label>Upload</label>
            <input type="file" className="custom-file-input" />
          </div> */}
          <div className="formGroup">
            <label>Name</label>
            <input
              type="text"
              placeholder="Item Name"
              autoFocus={true}
              onChange={(e) =>
                updateFormInput({ ...formInput, name: e.target.value })
              }
            />
          </div>
          <div className="formGroup">
            <label>Image Link</label>
            <input
              type="text"
              placeholder="insert image url"
              autoFocus={true}
              onChange={(e) =>
                updateFormInput({ ...formInput, image: e.target.value })
              }
            />
            {formInput.image && (
              <img className="rounded mt-4" width="350" src={formInput.image} />
            )}
          </div>
          <div className="formGroup">
            <label>Description</label>
            <textarea
              type="text"
              rows={4}
              placeholder="Decription of your item"
              onChange={(e) =>
                updateFormInput({ ...formInput, description: e.target.value })
              }
            ></textarea>
          </div>
          <div className="formGroup">
            <label>Price</label>
            <div className="twoForm">
              <input
                type="text"
                placeholder="Price"
                onChange={(e) =>
                  updateFormInput({ ...formInput, price: e.target.value })
                }
              />
              <select>
                <option value="ETH">ETH</option>
                <option value="BTC">BTC</option>
                <option value="LTC">LTC</option>
              </select>
            </div>
          </div>
          {/* <div className="formGroup">
            <label>Category</label>
            <select>
              <option>Art</option>
              <option>Photography</option>
              <option>Sports</option>
              <option>Collectibles</option>
              <option>Trading Cards</option>
              <option>Utility</option>
            </select>
          </div> */}
          {/* <div className="formGroup">
            <label>Available Items</label>
            <input type="text" placeholder="No of Items" />
          </div> */}
          <button className="writeButton" onClick={listNFTForSale}>
            Create Item
          </button>
        </form>
      </div>
    </div>
  );
};

export default Create;
